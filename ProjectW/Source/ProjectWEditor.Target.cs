// Copyright Petro Shoferystov, All Rights Reserved. XOXO

using UnrealBuildTool;
using System.Collections.Generic;

public class ProjectWEditorTarget : TargetRules
{
	public ProjectWEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "ProjectW" } );
	}
}
