﻿// Copyright Petro Shoferystov, All Rights Reserved. XOXO

#pragma once

#include "CoreMinimal.h"
#include "Characters/SUP_CharacterBase.h"
#include "Camera/CameraComponent.h"

#include "SUP_PlayerCharacter.generated.h"

class UCameraComponent;
class UMotionControllerComponent;

UCLASS()
class PROJECTW_API ASUP_PlayerCharacter : public ASUP_CharacterBase
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASUP_PlayerCharacter();

	
	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite, Category= "Controls|Input Mappings")
	class UInputMappingContext* BaseMappintContext;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite, Category= "Controls|Input Mappings")
	int32 BaseMappingPriority = 0;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class ASUP_HandController* LeftController;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class ASUP_HandController* RightController;
	
private:
	
	UPROPERTY(Category=Character, VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
	TObjectPtr<class UCameraComponent> Camera;

	UPROPERTY(VisibleAnywhere)
	class USceneComponent* VRRoot;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class ASUP_HandController> RightHandControllerClass;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class ASUP_HandController> LeftHandControllerClass;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

public:

	virtual void UnPossessed() override;

	virtual void Tick(float DeltaSeconds) override;
};
