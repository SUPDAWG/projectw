﻿// Copyright Petro Shoferystov, All Rights Reserved. XOXO

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SUP_PickupObject.generated.h"

UCLASS()
class PROJECTW_API ASUP_PickupObject : public AActor
{
	GENERATED_BODY()

public:
	//UPROPERTY(Category=Character, VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TObjectPtr<class UStaticMeshComponent> ObjectMesh;

	// Sets default values for this actor's properties
	ASUP_PickupObject();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
