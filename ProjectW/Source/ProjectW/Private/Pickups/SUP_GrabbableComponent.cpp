﻿// Copyright Petro Shoferystov, All Rights Reserved. XOXO


#include "Pickups/SUP_GrabbableComponent.h"

#include "GameFramework/Actor.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"
#include "Helpers/SUP_Helpers.h"
#include "Pickups/SUP_PickableObject.h"
#include "Pickups/SUP_GrabbablePivotComponent.h"


// Sets default values for this component's properties
USUP_GrabbableComponent::USUP_GrabbableComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}



// Called when the game starts
void USUP_GrabbableComponent::BeginPlay()
{
	Super::BeginPlay();

	GetComponentsByClass(GetOwner(),USUP_GrabbablePivotComponent::StaticClass(), GrabPivots);
	
	if(GetOwner()->Implements<USUP_PickableObject>())
	{
		GrabbablePrimitiveComponent = ISUP_PickableObject::Execute_GetPickupPrimitive(GetOwner());
	}
}

void USUP_GrabbableComponent::TickComponent(float DeltaTime, ELevelTick TickType,
	FActorComponentTickFunction* ThisTickFunction)
{
	if(GrabbablePrimitiveComponent && (bIsPicked || bIsForcePicked))
	{
		GrabbablePrimitiveComponent->WakeRigidBody();
	}
}

ESUP_GrabbingType USUP_GrabbableComponent::GetGrabbingType_Implementation(UPrimitiveComponent* GrabberPivot, EControllerHand ControllerHand)
{
	if((bCanBeGrabbedAnywhere && GrabbablePrimitiveComponent->IsOverlappingActor(GrabberPivot->GetOwner())) || FindGrabbablePivotComponent(GrabberPivot, ControllerHand) || !bCanBeGrabbedByForce)
	{
		return ESUP_GrabbingType::PhysicalGrab;
	}
	else
	{
		return ESUP_GrabbingType::ForceGrab;
	}
}

USUP_GrabbablePivotComponent* USUP_GrabbableComponent::FindGrabbablePivotComponent_Implementation(UPrimitiveComponent* Hand, EControllerHand ControllerHand)
{
	for (USUP_GrabbablePivotComponent* PickupPivot : GrabPivots)
	{
		if(PickupPivot->CanPickup(Hand, ControllerHand))
		{
			return PickupPivot;
		}
	}
	return nullptr;
}

bool USUP_GrabbableComponent::CanGrab_Implementation(UPrimitiveComponent* Hand, EControllerHand ControllerHand)
{
	return bCanBeGrabbedByForce || (bCanBeGrabbedAnywhere && GrabbablePrimitiveComponent->IsOverlappingActor(Hand->GetOwner())) || FindGrabbablePivotComponent(Hand, ControllerHand);
}


bool USUP_GrabbableComponent::Grab_Implementation(UPhysicsConstraintComponent* Constraint,
	UPrimitiveComponent* Hand, EControllerHand ControllerHand, ESUP_GrabbingType GrabbingType, FName PickableSocketName)
{
	GrabbablePrimitiveComponent->SetSimulatePhysics(true);
	//PickablePrimitiveComponent->SetRenderCustomDepth(true);
	//PickablePrimitiveComponent->SetCustomDepthStencilValue(1);
	
	if(GrabbingType == ESUP_GrabbingType::PhysicalGrab)
	{
		bIsPicked = true;

		if(!bCanBeGrabbedAnywhere)
		{
			USUP_GrabbablePivotComponent* PickupPivotComponent = FindGrabbablePivotComponent(Hand, ControllerHand);
			if(PickupPivotComponent)
			{
				if(AttachMethod==ESUP_AttachMethod::UseConstraints)
				{
					Hand->SetWorldLocation(PickupPivotComponent->GetComponentLocation() + PickupPivotComponent->GetForwardVector()*PickupPivotComponent->GetHandOffset(ControllerHand).X + PickupPivotComponent->GetRightVector()*PickupPivotComponent->GetHandOffset(ControllerHand).Y, false, nullptr, ETeleportType::ResetPhysics);
					Hand->SetWorldRotation(PickupPivotComponent->GetComponentRotation(), false, nullptr, ETeleportType::ResetPhysics);
				}
			}
			else
			{
				return false;
			}
			if(AttachMethod==ESUP_AttachMethod::UseConstraints)
			{
				Constraint->SetConstrainedComponents(Hand,FName(),GrabbablePrimitiveComponent, PickableSocketName);
				Constraints.AddUnique(Constraint);
			}
			else
			{
				GrabbablePrimitiveComponent->AttachToComponent(Hand,FAttachmentTransformRules(EAttachmentRule::SnapToTarget,EAttachmentRule::SnapToTarget,EAttachmentRule::KeepWorld, true));
				GrabbablePrimitiveComponent->AddWorldOffset(GrabbablePrimitiveComponent->GetComponentLocation() - PickupPivotComponent->GetComponentLocation());
				GrabbablePrimitiveComponent->AddWorldOffset( -(Hand->GetForwardVector()*PickupPivotComponent->GetHandOffset(ControllerHand).X + Hand->GetRightVector()*PickupPivotComponent->GetHandOffset(ControllerHand).Y));
				GrabbablePrimitiveComponent->SetWorldRotation(PickupPivotComponent->GetComponentRotation());
			}
		}
		else
		{
			if(AttachMethod==ESUP_AttachMethod::UseConstraints)
			{
				Constraint->SetConstrainedComponents(Hand,FName(),GrabbablePrimitiveComponent, PickableSocketName);
				Constraints.AddUnique(Constraint);

			}
			else
			{
				GrabbablePrimitiveComponent->AttachToComponent(Hand,FAttachmentTransformRules(EAttachmentRule::KeepWorld,EAttachmentRule::KeepWorld,EAttachmentRule::KeepWorld, true));
				//PickablePrimitiveComponent->AddWorldOffset(PickablePrimitiveComponent->GetComponentLocation() - PickupPivotComponent->GetComponentLocation());
				//PickablePrimitiveComponent->AddWorldOffset( -(Hand->GetForwardVector()*PickupPivotComponent->GetHandOffset(ControllerHand).X + Hand->GetRightVector()*PickupPivotComponent->GetHandOffset(ControllerHand).Y));
				//PickablePrimitiveComponent->SetWorldRotation(PickupPivotComponent->GetComponentRotation());
			}
		}
	}
	else if(GrabbingType == ESUP_GrabbingType::ForceGrab)
	{
		bIsForcePicked = true;
		//Constraint->SetConstrainedComponents(Hand,FName(),PickablePrimitiveComponent, PickableSocketName);
		//Constraints.AddUnique(Constraint);
		Constraint->SetConstrainedComponents(Hand,FName(),GrabbablePrimitiveComponent, PickableSocketName);
		Constraints.AddUnique(Constraint);
	}

	return true;
}

void USUP_GrabbableComponent::Drop_Implementation(UPhysicsConstraintComponent* Constraint, ESUP_GrabbingType GrabbingType)
{
	GrabbablePrimitiveComponent->SetRenderCustomDepth(false);
	GrabbablePrimitiveComponent->SetCustomDepthStencilValue(0);
	if(AttachMethod==ESUP_AttachMethod::UseConstraints || GrabbingType == ESUP_GrabbingType::ForceGrab)
	{
		Constraint->BreakConstraint();
		Constraints.Remove(Constraint);
		for (UPhysicsConstraintComponent* OldConstraint : Constraints)
		{
			UPrimitiveComponent* OutComponent1;
			FName OutBoneName1;
			UPrimitiveComponent* OutComponent2;
			FName OutBoneName2;
			OldConstraint->GetConstrainedComponents(OutComponent1, OutBoneName1, OutComponent2, OutBoneName2);
			OldConstraint->BreakConstraint();
			OldConstraint->SetConstrainedComponents(OutComponent1,OutBoneName1,OutComponent2, OutBoneName2);
		}

		if(bIsForcePicked)
		{
			bIsForcePicked = false;
			return;
		}

	}
	else
	{
		if(GrabbablePrimitiveComponent->GetAttachmentRootActor()==Constraint->GetOwner())
		{
			GrabbablePrimitiveComponent->DetachFromComponent(FDetachmentTransformRules(EDetachmentRule::KeepWorld,EDetachmentRule::KeepWorld,EDetachmentRule::KeepWorld,true));
		}
		else
		{
			return;
		}
	}
	if(Constraints.Num()==0)
	{
		//PickablePrimitiveComponent->DetachFromComponent(FDetachmentTransformRules(EDetachmentRule::KeepWorld,EDetachmentRule::KeepWorld,EDetachmentRule::KeepWorld,true));
		bIsPicked = false;
	}
}