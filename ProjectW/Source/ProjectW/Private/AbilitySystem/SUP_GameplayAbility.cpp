﻿// Copyright Petro Shoferystov, All Rights Reserved. XOXO


#include "AbilitySystem/SUP_GameplayAbility.h"

#include "EnhancedInputSubsystems.h"
#include "EnhancedPlayerInput.h"
#include "Kismet/GameplayStatics.h"

FString USUP_GameplayAbility::GetDebugName(const UGameplayAbility* Ability)
{
	if (Ability == nullptr)
		return TEXT("nullptr");

	return Ability->GetName();
}

void USUP_GameplayAbility::ExternalEndAbility()
{
	check(CurrentActorInfo);

	const bool bReplicateEndAbility = true;
	const bool bWasCancelled = false;
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, bReplicateEndAbility, bWasCancelled);
}

FInputActionValue USUP_GameplayAbility::GetInputValues()
{
	if(!EnhancedPlayerInput)
	{
		APlayerController* PlayerController = UGameplayStatics::GetPlayerController(this,0);
		EnhancedPlayerInput = Cast<UEnhancedPlayerInput>(PlayerController->PlayerInput);
	}
	if(EnhancedPlayerInput)
	{
		return EnhancedPlayerInput->FindActionInstanceData(AbilityInputAction)->GetValue();
	}
	return  FInputActionValue();
}

