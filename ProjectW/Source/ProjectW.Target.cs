// Copyright Petro Shoferystov, All Rights Reserved. XOXO

using UnrealBuildTool;
using System.Collections.Generic;

public class ProjectWTarget : TargetRules
{
	public ProjectWTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "ProjectW" } );
	}
}
