﻿// Copyright Petro Shoferystov, All Rights Reserved. XOXO

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "SUP_Enums.generated.h"

UENUM()
enum class ESUP_AttachMethod : uint8
{
	UseConstraints,
	AttachToHand
};

UENUM()
enum class ESUP_GrabbingType : uint8
{
	Any,
	PhysicalGrab,
	ForceGrab
};

