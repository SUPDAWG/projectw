﻿// Copyright Petro Shoferystov, All Rights Reserved. XOXO

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SUP_GameModeBase.generated.h"

UCLASS(BlueprintType)
class PROJECTW_API ASUP_GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
};
