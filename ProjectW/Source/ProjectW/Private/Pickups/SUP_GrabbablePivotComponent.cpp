﻿// Copyright Petro Shoferystov, All Rights Reserved. XOXO


#include "Pickups/SUP_GrabbablePivotComponent.h"


USUP_GrabbablePivotComponent::USUP_GrabbablePivotComponent()
{
	this->SetCollisionProfileName(TEXT("Interactable"));
	HandOffsets.Add(EControllerHand::Left,FVector(2.0f,-5.0f,0.0f));
	HandOffsets.Add(EControllerHand::Right,FVector(2.0f,5.0f,0.0f));
	SphereRadius = 16.0f;
}

bool USUP_GrabbablePivotComponent::CanPickup(UPrimitiveComponent* Hand, EControllerHand ControllerHand)
{
	return (RequiredHand==ControllerHand || RequiredHand == EControllerHand::AnyHand) && IsOverlappingComponent(Hand) && GetComponentRotation().Equals(Hand->GetComponentRotation(), PickupToleranceAngle);
}

FVector USUP_GrabbablePivotComponent::GetHandOffset(EControllerHand ControllerHand)
{
	FVector* HandOffset = HandOffsets.Find(ControllerHand);
	if(HandOffset)
	{
		return *HandOffset;
	}
	return FVector::ZeroVector;
}
