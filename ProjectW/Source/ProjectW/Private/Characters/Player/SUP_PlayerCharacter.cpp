﻿// Copyright Petro Shoferystov, All Rights Reserved. XOXO

#include "Characters/Player/SUP_PlayerCharacter.h"
#include "EnhancedInputSubsystems.h"
#include "MotionControllerComponent.h"
#include "Characters/Player/SUP_HandController.h"
#include "Kismet/KismetSystemLibrary.h"


// Sets default values
ASUP_PlayerCharacter::ASUP_PlayerCharacter()
{
	VRRoot = CreateDefaultSubobject<USceneComponent>(TEXT("VRRoot"));
	VRRoot->SetupAttachment(GetRootComponent());
	
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetupAttachment(VRRoot);
}

// Called when the game starts or when spawned
void ASUP_PlayerCharacter::BeginPlay()
{
	Super::BeginPlay();

    FActorSpawnParameters ControllerSpawnParameters;
    ControllerSpawnParameters.Owner=this;
    
    LeftController = GetWorld()->SpawnActor<ASUP_HandController>(LeftHandControllerClass);
    if(LeftController != nullptr)
    {
        LeftController->AttachToComponent(VRRoot, FAttachmentTransformRules::KeepRelativeTransform);
        LeftController->SetOwner(this);
        LeftController->SetHand(EControllerHand::Left);
    }
    
    RightController = GetWorld()->SpawnActor<ASUP_HandController>(RightHandControllerClass, ControllerSpawnParameters);
    if(RightController != nullptr)
    {
        RightController->AttachToComponent(VRRoot, FAttachmentTransformRules::KeepRelativeTransform);
        RightController->SetOwner(this);
        RightController->SetHand(EControllerHand::Right);
    }
    
    LeftController->PairController(RightController);
    RightController->PairController(LeftController);
}

void ASUP_PlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	APlayerController* PlayerController = Cast<APlayerController>(GetController());
	if(PlayerController)
	{
		if(UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->ClearAllMappings();
			Subsystem->AddMappingContext(BaseMappintContext, BaseMappingPriority);

			AbilitySystemComponent->BindAbilityActivationToEnhancedInputComponent(InputComponent);
		}
	}
}

void ASUP_PlayerCharacter::UnPossessed()
{
	Super::UnPossessed();

	AbilitySystemComponent->UnbindAbilityActivateFromEnhancedInputComponent(InputComponent);
}

void ASUP_PlayerCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);


	TArray<AActor*> IgnoredActors;
	IgnoredActors.Add(LeftController);
	IgnoredActors.Add(RightController);
	FHitResult OutHit;
	if(!UKismetSystemLibrary::SphereTraceSingle(this,GetActorLocation(),Camera->GetComponentLocation(),25,ETraceTypeQuery::TraceTypeQuery1,false,IgnoredActors,EDrawDebugTrace::ForOneFrame,OutHit,false))
	{
		FVector NewCameraOffset = Camera->GetComponentLocation()-GetActorLocation();
		NewCameraOffset.Z = 0;
		AddActorWorldOffset(NewCameraOffset);
		VRRoot->AddWorldOffset(-NewCameraOffset);
	}

}

