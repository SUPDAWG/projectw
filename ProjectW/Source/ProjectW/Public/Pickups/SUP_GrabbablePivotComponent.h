﻿// Copyright Petro Shoferystov, All Rights Reserved. XOXO

#pragma once

#include "CoreMinimal.h"

#include "Animation/AnimMontage.h"
#include "Components/SphereComponent.h"

#include "SUP_GrabbablePivotComponent.generated.h"

/**
* 
*/
UCLASS(Blueprintable, meta=(BlueprintSpawnableComponent))
class PROJECTW_API USUP_GrabbablePivotComponent : public USphereComponent
{
	GENERATED_BODY()

public:

	USUP_GrabbablePivotComponent();
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Pickup Info")
	EControllerHand RequiredHand = EControllerHand::AnyHand;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Pickup Info")
	TMap<EControllerHand,UAnimMontage*> GripMontages;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Pickup Info")
	float PickupToleranceAngle = 30.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Pickup Info")
	TMap<EControllerHand,FVector> HandOffsets;
	
	UFUNCTION(BlueprintCallable)
	bool CanPickup(UPrimitiveComponent* Hand, EControllerHand ControllerHand);

	UFUNCTION(BlueprintCallable)
	FVector GetHandOffset(EControllerHand ControllerHand);
};
