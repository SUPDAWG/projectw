﻿// Copyright Petro Shoferystov, All Rights Reserved. XOXO

#pragma once

#include "CoreMinimal.h"
#include "InputCoreTypes.h"
#include "Components/ActorComponent.h"
#include "Helpers/SUP_Enums.h"

#include "SUP_GrabbableComponent.generated.h"

class UPhysicsConstraintComponent;
class USUP_GrabbablePivotComponent;

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class PROJECTW_API USUP_GrabbableComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	USUP_GrabbableComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:

	UPROPERTY(BlueprintReadOnly, Category="SUP Grabbable")
	TArray<UPhysicsConstraintComponent*> Constraints;

	UPROPERTY(BlueprintReadOnly, Category="SUP Grabbable")
	TArray<USUP_GrabbablePivotComponent*> GrabPivots;

	UPROPERTY(BlueprintReadOnly, Category="SUP Grabbable")
	UPrimitiveComponent* GrabbablePrimitiveComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="SUP Grabbable")
	ESUP_AttachMethod AttachMethod;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="SUP Grabbable")
	bool bCanBeGrabbedAnywhere;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="SUP Grabbable")
	bool bCanBeGrabbedByForce;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="SUP Grabbable")
	bool bCanBeDroppedIfHandControllerIsFarAway = true;

	UPROPERTY(BlueprintReadOnly, Category="SUP Grabbable")
	bool bIsPicked;

	UPROPERTY(BlueprintReadOnly, Category="SUP Grabbable")
	bool bIsForcePicked;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SUP Grabbable")
	bool CanGrab(UPrimitiveComponent* HandMesh, EControllerHand ControllerHand);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SUP Grabbable")
	ESUP_GrabbingType GetGrabbingType(UPrimitiveComponent* GrabberPivot, EControllerHand ControllerHand);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SUP Grabbable")
	bool Grab(UPhysicsConstraintComponent* Constraint, UPrimitiveComponent* Hand, EControllerHand ControllerHand, ESUP_GrabbingType GrabbingType, FName PickableSocketName = "None");
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SUP Grabbable")
	void Drop(class UPhysicsConstraintComponent* Constraint, ESUP_GrabbingType GrabbingType);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SUP Grabbable")
	class USUP_GrabbablePivotComponent* FindGrabbablePivotComponent(UPrimitiveComponent* Hand, EControllerHand ControllerHand);

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
};
