﻿// Copyright Petro Shoferystov, All Rights Reserved. XOXO

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AbilitySystemInterface.h"
#include "AbilitySystem/SUP_AbilitySystemComponent.h"

#include "SUP_CharacterBase.generated.h"

UCLASS()
class PROJECTW_API ASUP_CharacterBase
	: public ACharacter
	, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASUP_CharacterBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	USUP_AbilitySystemComponent* AbilitySystemComponent;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	UAbilitySystemComponent* GetAbilitySystemComponent() const override;

};
