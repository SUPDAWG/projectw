﻿// Copyright Petro Shoferystov, All Rights Reserved. XOXO

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SUP_HandController.generated.h"

UCLASS()
class PROJECTW_API ASUP_HandController : public AActor
{
	GENERATED_BODY()
	
	public:	
	// Sets default values for this actor's properties
	ASUP_HandController();

	protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	public:	

	void SetHand(EControllerHand Hand);

	void PairController(ASUP_HandController* NewController) {OtherController = NewController;}
		
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class USceneComponent* HandRoot;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UMotionControllerComponent* MotionController;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class USphereComponent* HandCollision;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UStaticMeshComponent* HandPivot;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UPhysicsConstraintComponent* PhysicsConstraint;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UPhysicsConstraintComponent* GrabPhysicsConstraint;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class USkeletalMeshComponent* HandMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UStaticMeshComponent* TelekineticPivot;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UPhysicsConstraintComponent* TelekineticPhysicsConstraint;
	
	class APlayerController* PlayerController;

	ASUP_HandController* OtherController;
};
