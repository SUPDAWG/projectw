﻿// Copyright Petro Shoferystov, All Rights Reserved. XOXO

#pragma once

#include "AttributeSet.h"
#include "SUP_AttributeSet.generated.h"


UCLASS()
class PROJECTW_API USUP_AttributeSet : public UAttributeSet
{
	GENERATED_BODY()
};
