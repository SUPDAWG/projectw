﻿// Copyright Petro Shoferystov, All Rights Reserved. XOXO


#include "AbilitySystem/SUP_GameplayAbilitySet.h"
#include "AbilitySystem/SUP_GameplayAbility.h"
#include "AbilitySystemComponent.h"
#include "AbilitySystemGlobals.h"
#include "Debug/SUP_Debug.h"
#include "GameFramework/Actor.h"
#include "GameplayEffect.h"

void USUP_GameplayAbilitySet::Give(UAbilitySystemComponent* AbilitySystemComponent, UObject* SourceObject, int32 Level, FSUP_GameplayAbilitySetHandles& OutAbilitySetHandles) const
{
	if (AbilitySystemComponent == nullptr)
	{
		UE_LOG(LogSupCommon, Error, TEXT("Tried to give AxGameplayAbilitySet to a null AbilitySystemComponent"));
		return;
	}

	for (const TSubclassOf<UGameplayAbility>& AbilityClass : Abilities)
	{
		if (!AbilityClass)
		{
			UE_LOG(LogSupCommon, Error, TEXT("Tried to give a null ability while granting abilities to %s"), *AActor::GetDebugName(AbilitySystemComponent->GetOwner()));
			continue;
		}
		int32 inputID = INDEX_NONE;
		FGameplayAbilitySpecHandle AbilitySpecHandle = AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(AbilityClass, Level, inputID, SourceObject));
		OutAbilitySetHandles.AbilityHandles.Add(AbilitySpecHandle);
	}

	for (const TSubclassOf<UGameplayEffect>& GameplayEffectClass : Effects)
	{
		if (!GameplayEffectClass)
		{
			UE_LOG(LogSupCommon, Error, TEXT("Tried to give a null ability while granting abilities to %s"), *AActor::GetDebugName(AbilitySystemComponent->GetOwner()));
			continue;
		}

		FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
		if (SourceObject) EffectContext.AddSourceObject(SourceObject);

		FGameplayEffectSpecHandle NewHandle = AbilitySystemComponent->MakeOutgoingSpec(GameplayEffectClass, Level, EffectContext);
		if (NewHandle.IsValid())
		{
			FActiveGameplayEffectHandle ActiveGEHandle = AbilitySystemComponent->ApplyGameplayEffectSpecToTarget(*NewHandle.Data.Get(), AbilitySystemComponent);

			// we only keep track of EGameplayEffectDurationType::Infinite effects, we'll assume ::Instant and ::HasDuration
			// we'll sort themselves out
			if (GameplayEffectClass.GetDefaultObject()->DurationPolicy == EGameplayEffectDurationType::Infinite)
				OutAbilitySetHandles.EffectHandles.Add(ActiveGEHandle);
		}
	}
}

void USUP_GameplayAbilitySet::Give(AActor* Actor, UObject* SourceObject, int32 Level, FSUP_GameplayAbilitySetHandles& OutAbilitySetHandles) const
{
	Give(UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(Actor), SourceObject, Level, OutAbilitySetHandles);
}

void USUP_GameplayAbilitySet::Give(UAbilitySystemComponent* AbilitySystemComponent, UObject* SourceObject, int32 Level) const
{
	FSUP_GameplayAbilitySetHandles DiscardHandles;
	Give(AbilitySystemComponent, SourceObject, Level, DiscardHandles);
}

void USUP_GameplayAbilitySet::Give(AActor* Actor, UObject* SourceObject, int32 Level) const
{
	Give(UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(Actor), SourceObject, Level);
}

void USUP_GameplayAbilitySet::Remove(UAbilitySystemComponent* AbilitySystemComponent, FSUP_GameplayAbilitySetHandles& AbilitySetHandles) const
{
	if (!IsValid(AbilitySystemComponent))
	{
		UE_LOG(LogSupCommon, Error, TEXT("Tried to remove AxGameplayAbilitySet from a null AbilitySystemComponent"));
		return;
	}

	if (AbilitySetHandles.EffectHandles.Num() > 0)
	{
		// we'll only remove the abilities that are actually removed
		TArray<FActiveGameplayEffectHandle>& EffectHandles = AbilitySetHandles.EffectHandles;
		for (int EffectIndex = EffectHandles.Num() - 1; EffectIndex >= 0; --EffectIndex)
		{
			const FActiveGameplayEffectHandle& EffectHandle = EffectHandles[EffectIndex];
			if (AbilitySystemComponent->RemoveActiveGameplayEffect(EffectHandle))
			{
				EffectHandles.RemoveAt(EffectIndex);
			}
		}
	}

	if (AbilitySetHandles.AbilityHandles.Num() > 0)
	{
		// there's no way for us to know if it was actually removed, so we'll empty them all,
		// and then just empty the list
		for (const FGameplayAbilitySpecHandle& AbilityHandle : AbilitySetHandles.AbilityHandles)
		{
			AbilitySystemComponent->ClearAbility(AbilityHandle);
		}
		AbilitySetHandles.AbilityHandles.Empty();
	}
}

void USUP_GameplayAbilitySet::Remove(AActor* Actor, FSUP_GameplayAbilitySetHandles& AbilitySetHandles) const
{
	Remove(UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(Actor), AbilitySetHandles);
}