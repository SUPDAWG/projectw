﻿// Copyright Petro Shoferystov, All Rights Reserved. XOXO


#include "Pickups/SUP_PickupObject.h"


// Sets default values
ASUP_PickupObject::ASUP_PickupObject()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ObjectMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ObjectMesh"));
	SetRootComponent(ObjectMesh);
	
}

// Called when the game starts or when spawned
void ASUP_PickupObject::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASUP_PickupObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

