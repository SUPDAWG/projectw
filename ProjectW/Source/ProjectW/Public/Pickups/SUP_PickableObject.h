﻿// Copyright Petro Shoferystov, All Rights Reserved. XOXO

#pragma once

#include "CoreMinimal.h"
#include "InputCoreTypes.h"
#include "UObject/Interface.h"
#include "SUP_PickableObject.generated.h"

// This class does not need to be modified.
UINTERFACE()
class USUP_PickableObject : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class PROJECTW_API ISUP_PickableObject
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SUP Pickable")
	UPrimitiveComponent* GetPickupPrimitive();
};
