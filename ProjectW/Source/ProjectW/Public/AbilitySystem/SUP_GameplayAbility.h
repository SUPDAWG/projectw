﻿// Copyright Petro Shoferystov, All Rights Reserved. XOXO

#pragma once

#include "CoreMinimal.h"

#include "InputAction.h"
#include "Abilities/GameplayAbility.h"
#include "SUP_GameplayAbility.generated.h"

/**
 * 
 */
UCLASS(Abstract)
class PROJECTW_API USUP_GameplayAbility : public UGameplayAbility
{
	GENERATED_BODY()

	public:

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Input", meta=(DisplayName="Active on InputAction"))
	bool bActivateAbilityOnInputAction = true;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Input")
	const UInputAction* AbilityInputAction;

	static FString GetDebugName(const UGameplayAbility* Ability);

	void ExternalEndAbility();

	UFUNCTION(BlueprintCallable)
	FInputActionValue GetInputValues();

	UPROPERTY()
	TObjectPtr<UEnhancedPlayerInput> EnhancedPlayerInput;
	
};
