﻿// Copyright Petro Shoferystov, All Rights Reserved. XOXO


#include "Characters/SUP_CharacterBase.h"

#include "AbilitySystem/SUP_GameplayAbilitySet.h"
#include "Camera/CameraComponent.h"


// Sets default values
ASUP_CharacterBase::ASUP_CharacterBase()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AbilitySystemComponent = CreateDefaultSubobject<USUP_AbilitySystemComponent>(TEXT("AbilitySystemComponent"));
	
}

// Called when the game starts or when spawned
void ASUP_CharacterBase::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ASUP_CharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

UAbilitySystemComponent* ASUP_CharacterBase::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

