﻿// Copyright Petro Shoferystov, All Rights Reserved. XOXO

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"

#include "SUP_Helpers.generated.h"

class UActorComponent;

UCLASS()
class PROJECTW_API USUP_Helpers : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	
};

template <typename T, typename TAllocator>
void GetComponentsByClass(AActor* Actor, TSubclassOf<UActorComponent> Class, TArray<T*, TAllocator>& OutArray)
{
#if ENGINE_MINOR_VERSION < 25
	const TArray<UActorComponent*> ActorComponents = Actor->GetComponentsByClass(Class);
#else
	TInlineComponentArray<UActorComponent*> ActorComponents;
	Actor->GetComponents(Class, ActorComponents);
#endif

	OutArray.SetNumZeroed(ActorComponents.Num());

	for (int32 i = 0; i < ActorComponents.Num(); ++i)
	{
		OutArray[i] = Cast<T>(ActorComponents[i]);
	}
}
