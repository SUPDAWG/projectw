﻿// Copyright Petro Shoferystov, All Rights Reserved. XOXO

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "EnhancedInputComponent.h"
#include "InputAction.h"

#include "SUP_AbilitySystemComponent.generated.h"


UCLASS()
class PROJECTW_API USUP_AbilitySystemComponent : public UAbilitySystemComponent
{
	GENERATED_BODY()

public:

	void BindAbilityActivationToEnhancedInputComponent(UInputComponent* InputComponent);
	void UnbindAbilityActivateFromEnhancedInputComponent(UInputComponent* InputComponent);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Abilities")
	FGameplayAbilitySpecHandle FindAbilitySpecHandleForClass(TSubclassOf<UGameplayAbility> AbilityClass, UObject* OptionalSourceObject = nullptr);
	
	UFUNCTION(BlueprintCallable, Category = "Abilities")
	virtual bool TryActivateAbilityBatched(FGameplayAbilitySpecHandle InAbilityHandle, bool EndAbilityImmediately);

	UFUNCTION(BlueprintCallable, Category = "Abilities", Meta = (DisplayName = "AddLooseGameplayTag"))
	void K2_AddLooseGameplayTag(const FGameplayTag& GameplayTag, int32 Count = 1);

	UFUNCTION(BlueprintCallable, Category = "Abilities", Meta = (DisplayName = "AddLooseGameplayTags"))
	void K2_AddLooseGameplayTags(const FGameplayTagContainer& GameplayTags, int32 Count = 1);

	UFUNCTION(BlueprintCallable, Category = "Abilities", Meta = (DisplayName = "RemoveLooseGameplayTag"))
	void K2_RemoveLooseGameplayTag(const FGameplayTag& GameplayTag, int32 Count = 1);

	UFUNCTION(BlueprintCallable, Category = "Abilities", Meta = (DisplayName = "RemoveLooseGameplayTags"))
	void K2_RemoveLooseGameplayTags(const FGameplayTagContainer& GameplayTags, int32 Count = 1);

	UFUNCTION(Category = "Abilities", BlueprintPure)
	bool GetAbilityHandleByClass(TSubclassOf<UGameplayAbility> AbilityClass, FGameplayAbilitySpecHandle& AbilitySpecHandleOUT);

	UFUNCTION(Category = "Abilities", BlueprintPure)
	bool GetAbilityInstanceByClass(TSubclassOf<UGameplayAbility> AbilityClass, UGameplayAbility*& AbilityInstanceOUT);
	
	UPROPERTY(EditAnywhere,BlueprintReadOnly, Category = "Abilities")
	class USUP_GameplayAbilitySet* StartupAbilitySet;

	virtual void BeginPlay() override;
private:

	void OnInputActionStarted(const UInputAction* InputAction);
	void OnInputActionTriggered(const UInputAction* InputAction);
	void OnInputActionEnded(const UInputAction* InputAction);

	void CacheInputActions();

	bool bHasCachedInputActions;

	UPROPERTY()
	TArray<TObjectPtr<const class UInputAction>> CachedInputActions;

	TMap<UInputComponent*, TArray<uint32>> BoundActionsByComponent;
};
