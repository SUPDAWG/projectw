﻿// Copyright Petro Shoferystov, All Rights Reserved. XOXO

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "GameplayAbilitySpec.h"
#include "GameplayEffectTypes.h"
#include "SUP_GameplayAbilitySet.generated.h"

USTRUCT()
struct FSUP_GameplayAbilitySetHandles
{
	GENERATED_BODY()

public:
	bool Any() const { return (AbilityHandles.Num() > 0) || (EffectHandles.Num() > 0); }

protected:
	UPROPERTY()
	TArray<FGameplayAbilitySpecHandle> AbilityHandles;

	UPROPERTY()
	TArray<FActiveGameplayEffectHandle> EffectHandles;

	friend class USUP_GameplayAbilitySet;
};

UCLASS()
class PROJECTW_API USUP_GameplayAbilitySet : public UDataAsset
{
	GENERATED_BODY()

	public:
	void Give(class UAbilitySystemComponent* AbilitySystemComponent, UObject* SourceObject, int32 Level, FSUP_GameplayAbilitySetHandles& OutAbilitySetHandles) const;
	void Give(class AActor* Actor, UObject* SourceObject, int32 Level, FSUP_GameplayAbilitySetHandles& OutAbilitySetHandles) const;
	void Give(class UAbilitySystemComponent* AbilitySystemComponent, UObject* SourceObject = nullptr, int32 Level = 1) const;
	void Give(class AActor* Actor, UObject* SourceObject = nullptr, int32 Level = 1) const;

	void Remove(class UAbilitySystemComponent* AbilitySystemComponent, FSUP_GameplayAbilitySetHandles& AbilitySetHandles) const;
	void Remove(class AActor* Actor, FSUP_GameplayAbilitySetHandles& AbilitySetHandles) const;

	protected:
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Abilities")
	TArray<TSubclassOf<class UGameplayAbility>> Abilities;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Effects")
	TArray<TSubclassOf<class UGameplayEffect>> Effects;
};
