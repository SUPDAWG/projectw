﻿// Copyright Petro Shoferystov, All Rights Reserved. XOXO


#include "AbilitySystem/SUP_AbilitySystemComponent.h"
#include "EnhancedInputComponent.h"
#include "AbilitySystem/SUP_GameplayAbility.h"
#include "AbilitySystem/SUP_GameplayAbilitySet.h"
#include "AssetRegistry/AssetRegistryModule.h"
#include "Debug/SUP_Debug.h"
#include "Kismet/GameplayStatics.h"
#include "EnhancedPlayerInput.h"

void USUP_AbilitySystemComponent::BindAbilityActivationToEnhancedInputComponent(UInputComponent* InputComponent)
{
	UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(InputComponent);

	if (EnhancedInputComponent)
	{
		CacheInputActions();

		TArray<uint32>& BoundActions = BoundActionsByComponent.FindOrAdd(EnhancedInputComponent);
		for (const UInputAction* InputAction : CachedInputActions)
		{
			// probably don't need Started since Triggered will get called
			//BoundActions.Add(EnhancedInputComponent->BindAction(InputAction, ETriggerEvent::Started, this, &UAxAbilitySystemComponent::OnInputActionStarted, InputifAction).GetHandle());

			BoundActions.Add(EnhancedInputComponent->BindAction(InputAction, ETriggerEvent::Triggered, this, &USUP_AbilitySystemComponent::OnInputActionTriggered, InputAction).GetHandle());
			BoundActions.Add(EnhancedInputComponent->BindAction(InputAction, ETriggerEvent::Completed, this, &USUP_AbilitySystemComponent::OnInputActionEnded, InputAction).GetHandle());
			BoundActions.Add(EnhancedInputComponent->BindAction(InputAction, ETriggerEvent::Canceled, this, &USUP_AbilitySystemComponent::OnInputActionEnded, InputAction).GetHandle());
		}
	}
}

void USUP_AbilitySystemComponent::UnbindAbilityActivateFromEnhancedInputComponent(UInputComponent* InputComponent)
{
	UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(InputComponent);

	if (EnhancedInputComponent)
	{
		CacheInputActions();

		TArray<uint32>* BoundActionsPtr = BoundActionsByComponent.Find(EnhancedInputComponent);
		if (BoundActionsPtr)
		{
			for (uint32 handle : *BoundActionsPtr)
			{
				EnhancedInputComponent->RemoveBindingByHandle(handle);
			}
		}
	}
}

void USUP_AbilitySystemComponent::BeginPlay()
{
	Super::BeginPlay();

	if(StartupAbilitySet)
	{
		StartupAbilitySet->Give(GetOwnerActor());
	}
}

void USUP_AbilitySystemComponent::OnInputActionStarted(const UInputAction* InputAction)
{
	ABILITYLIST_SCOPE_LOCK();
	for (FGameplayAbilitySpec& Spec : ActivatableAbilities.Items)
	{
		if (Spec.Ability)
		{
			USUP_GameplayAbility* SupGameplayAbility = Cast<USUP_GameplayAbility>(Spec.Ability);
			if (SupGameplayAbility && SupGameplayAbility->AbilityInputAction == InputAction)
			{
				Spec.InputPressed = true;
	
				if (Spec.IsActive())
				{
					if (Spec.Ability->bReplicateInputDirectly && IsOwnerActorAuthoritative() == false)
					{
						ServerSetInputPressed(Spec.Handle);
					}

					AbilitySpecInputPressed(Spec);

					// Invoke the InputPressed event. This is not replicated here. If someone is listening, they may replicate the InputPressed event to the server.
					InvokeReplicatedEvent(EAbilityGenericReplicatedEvent::InputPressed, Spec.Handle, Spec.ActivationInfo.GetActivationPredictionKey());
				}
				else if (SupGameplayAbility->bActivateAbilityOnInputAction)
				{
					// Ability is not active, so try to activate it
					if(!TryActivateAbility(Spec.Handle))
					{
						UE_LOG(LogSupCommon, VeryVerbose, TEXT("OnInputActionStarted: Failed to activate %s"), *USUP_GameplayAbility::GetDebugName(Spec.Ability));
					}
				}
			}
		}
	}
}

void USUP_AbilitySystemComponent::OnInputActionTriggered(const UInputAction* InputAction)
{
	OnInputActionStarted(InputAction);
}

void USUP_AbilitySystemComponent::OnInputActionEnded(const UInputAction* InputAction)
{
	ABILITYLIST_SCOPE_LOCK();
	for (FGameplayAbilitySpec& Spec : ActivatableAbilities.Items)
	{
		if (Spec.Ability)
		{
			USUP_GameplayAbility* SupGameplayAbility = Cast<USUP_GameplayAbility>(Spec.Ability);
			if (SupGameplayAbility && SupGameplayAbility->AbilityInputAction == InputAction)
			{
				Spec.InputPressed = false;
				if (Spec.Ability && Spec.IsActive())
				{
					if (Spec.Ability->bReplicateInputDirectly && IsOwnerActorAuthoritative() == false)
					{
						ServerSetInputReleased(Spec.Handle);
					}

					AbilitySpecInputReleased(Spec);

					InvokeReplicatedEvent(EAbilityGenericReplicatedEvent::InputReleased, Spec.Handle, Spec.ActivationInfo.GetActivationPredictionKey());
				}
			}
		}
	}
}

void USUP_AbilitySystemComponent::CacheInputActions()
{
	if (bHasCachedInputActions)
		return;
	
	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(FName("AssetRegistry"));
	IAssetRegistry& AssetRegistry = AssetRegistryModule.Get();

	FARFilter Filter;
	Filter.ClassNames = { TEXT("InputAction") };
	Filter.bRecursiveClasses = false;

	TArray<FAssetData> AssetList;
	
	// This data could be stored as static and processed in Default___CharacterClass's UObject,
	// but I think the performance hit is negligible and it's easier this way.
	AssetRegistry.GetAssets(Filter, AssetList);
	for (const FAssetData& Asset : AssetList)
	{
		UObject* Obj = Asset.GetAsset();
		const UInputAction* InputAction = Cast<UInputAction>(Obj);
		if (InputAction)
			CachedInputActions.Add(InputAction);
	}
}

FGameplayAbilitySpecHandle USUP_AbilitySystemComponent::FindAbilitySpecHandleForClass(
	TSubclassOf<UGameplayAbility> AbilityClass, UObject* OptionalSourceObject)
{
	ABILITYLIST_SCOPE_LOCK();
	for (FGameplayAbilitySpec& Spec : ActivatableAbilities.Items)
	{
		TSubclassOf<UGameplayAbility> SpecAbilityClass = Spec.Ability->GetClass();
		if (SpecAbilityClass == AbilityClass)
		{
			if (!OptionalSourceObject || (OptionalSourceObject && Spec.SourceObject == OptionalSourceObject))
			{
				return Spec.Handle;
			}
		}
	}

	return FGameplayAbilitySpecHandle();
}

bool USUP_AbilitySystemComponent::TryActivateAbilityBatched(FGameplayAbilitySpecHandle InAbilityHandle,
	bool EndAbilityImmediately)
{
	bool AbilityActivated = false;
	if (InAbilityHandle.IsValid())
	{
		FScopedServerAbilityRPCBatcher GSAbilityRPCBatcher(this, InAbilityHandle);
		AbilityActivated = TryActivateAbility(InAbilityHandle, true);

		if (EndAbilityImmediately)
		{
			FGameplayAbilitySpec* AbilitySpec = FindAbilitySpecFromHandle(InAbilityHandle);
			if (AbilitySpec)
			{
				USUP_GameplayAbility* Ability = Cast<USUP_GameplayAbility>(AbilitySpec->GetPrimaryInstance());
				Ability->ExternalEndAbility();
			}
		}

		return AbilityActivated;
	}

	return AbilityActivated;
}

void USUP_AbilitySystemComponent::K2_AddLooseGameplayTag(const FGameplayTag& GameplayTag, int32 Count)
{
	AddLooseGameplayTag(GameplayTag, Count);
}

void USUP_AbilitySystemComponent::K2_AddLooseGameplayTags(const FGameplayTagContainer& GameplayTags, int32 Count)
{
	AddLooseGameplayTags(GameplayTags, Count);
}

void USUP_AbilitySystemComponent::K2_RemoveLooseGameplayTag(const FGameplayTag& GameplayTag, int32 Count)
{
	RemoveLooseGameplayTag(GameplayTag, Count);
}

void USUP_AbilitySystemComponent::K2_RemoveLooseGameplayTags(const FGameplayTagContainer& GameplayTags, int32 Count)
{
	RemoveLooseGameplayTags(GameplayTags, Count);
}

bool USUP_AbilitySystemComponent::GetAbilityHandleByClass(TSubclassOf<UGameplayAbility> AbilityClass,
	FGameplayAbilitySpecHandle& AbilitySpecHandleOUT)
{
	UGameplayAbility* AbilityCDO = Cast<UGameplayAbility>(AbilityClass->GetDefaultObject());
	for (const FGameplayAbilitySpec& Spec : GetActivatableAbilities())
	{
		if (Spec.Ability == AbilityCDO)
		{
			AbilitySpecHandleOUT = Spec.Handle;
			return true;
		}
	}
	return false;
}

bool USUP_AbilitySystemComponent::GetAbilityInstanceByClass(TSubclassOf<UGameplayAbility> AbilityClass,
	UGameplayAbility*& AbilityInstanceOUT)
{
	UGameplayAbility* AbilityCDO = Cast<UGameplayAbility>(AbilityClass->GetDefaultObject());
	for (const FGameplayAbilitySpec& Spec : GetActivatableAbilities())
	{
		if (Spec.Ability == AbilityCDO)
		{
			AbilityInstanceOUT = Spec.GetPrimaryInstance();
			return true;
		}
	}
	return false;
}
