﻿// Copyright Petro Shoferystov, All Rights Reserved. XOXO


#include "Characters/Player/SUP_HandController.h"

#include "MotionControllerComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"


// Sets default values
ASUP_HandController::ASUP_HandController()
{
	PrimaryActorTick.bCanEverTick = true;

	HandRoot = CreateDefaultSubobject<USceneComponent>(TEXT("VRRoot"));
	SetRootComponent(HandRoot);
	
	MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("MotionController"));
	MotionController->SetCollisionProfileName(TEXT("OverlapAll"));
	MotionController->SetupAttachment(GetRootComponent());

	HandCollision = CreateDefaultSubobject<USphereComponent>(TEXT("HandCollision"));
	HandCollision->SetupAttachment(MotionController);
	HandCollision->SetSphereRadius(10);

	PhysicsConstraint = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("PhysicsConstraint"));
	PhysicsConstraint->SetDisableCollision(true);
	PhysicsConstraint->ConstraintInstance.DisableProjection();
	PhysicsConstraint->SetLinearXLimit(ELinearConstraintMotion::LCM_Limited,90);
	PhysicsConstraint->SetLinearYLimit(ELinearConstraintMotion::LCM_Limited,90);
	PhysicsConstraint->SetLinearZLimit(ELinearConstraintMotion::LCM_Limited,90);
	PhysicsConstraint->SetAngularSwing1Limit(EAngularConstraintMotion::ACM_Locked,45);
	PhysicsConstraint->SetAngularSwing2Limit(EAngularConstraintMotion::ACM_Locked,45);
	PhysicsConstraint->SetAngularTwistLimit(EAngularConstraintMotion::ACM_Locked,45);
	PhysicsConstraint->SetAngularDriveMode(EAngularDriveMode::TwistAndSwing);
	PhysicsConstraint->SetupAttachment(MotionController);

	GrabPhysicsConstraint = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("GrabPhysicsConstraint"));
	GrabPhysicsConstraint->SetAngularSwing1Limit(EAngularConstraintMotion::ACM_Locked,45);
	GrabPhysicsConstraint->SetAngularSwing2Limit(EAngularConstraintMotion::ACM_Locked,45);
	GrabPhysicsConstraint->SetAngularTwistLimit(EAngularConstraintMotion::ACM_Locked,45);
	GrabPhysicsConstraint->SetupAttachment(MotionController); 
	
	HandMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("HandMesh"));
	HandMesh->SetSimulatePhysics(true);
	HandMesh->SetCollisionProfileName(TEXT("PhysicsActor"));
	HandMesh->CastShadow=false;
	HandMesh->SetupAttachment(HandRoot);

	HandPivot = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("HandPivot"));
	HandPivot->SetGenerateOverlapEvents(false);
	HandPivot->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	HandPivot->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	HandPivot->SetupAttachment(MotionController);

	TelekineticPivot = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("TelekineticPivot"));
	TelekineticPivot->SetGenerateOverlapEvents(false);
	TelekineticPivot->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	TelekineticPivot->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	TelekineticPivot->SetupAttachment(HandRoot);

	TelekineticPhysicsConstraint = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("TelekineticPhysicsConstraint"));
	TelekineticPhysicsConstraint->SetAngularSwing1Limit(EAngularConstraintMotion::ACM_Free,45);
	TelekineticPhysicsConstraint->SetAngularSwing2Limit(EAngularConstraintMotion::ACM_Free,45);
	TelekineticPhysicsConstraint->SetAngularTwistLimit(EAngularConstraintMotion::ACM_Free,45);
	TelekineticPhysicsConstraint->SetLinearXLimit(ELinearConstraintMotion::LCM_Free,0.0f);
	TelekineticPhysicsConstraint->SetLinearYLimit(ELinearConstraintMotion::LCM_Free,0.0f);
	TelekineticPhysicsConstraint->SetLinearZLimit(ELinearConstraintMotion::LCM_Free,0.0f);
	TelekineticPhysicsConstraint->SetupAttachment(TelekineticPivot); 
	
}

// Called when the game starts or when spawned
void ASUP_HandController::BeginPlay()
{
	Super::BeginPlay();

	PlayerController = GetWorld()->GetFirstPlayerController();

	AActor* ParentActor = GetAttachParentActor();
	if(ParentActor)
	{
		APawn* OwningPawn = Cast<APawn>(ParentActor);
		if(OwningPawn)
		{
			PlayerController = Cast<APlayerController>(OwningPawn->GetController());
		}
	}

	PhysicsConstraint->SetConstrainedComponents(HandPivot,TEXT("None"), HandMesh, TEXT("None"));
}

void ASUP_HandController::SetHand(EControllerHand Hand)
{
	MotionController->SetTrackingSource(Hand);
	MotionController->bDisplayDeviceModel = true;
}

